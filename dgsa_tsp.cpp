// dgsa_tsp.cpp : This file contains the 'main' function. Program execution begins and ends there.
//


#include <iostream>
#include <math.h>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <cstdlib>
#include <iomanip>
#include <map>


using namespace std;




//input file format: city_index x_position y_position
string FILE_PATH = "";
string FILE_NAME = "berlin52.tsp";
double FLOAT_EQ_CT = 0.00001;
int MAX_NR_ITERATIONS = 100;
pair<int, int> EVERY_X_ITERATIONS_DO_Y_2OPT_MOVEMENTS;
int VELOCITY_DECREASE_FACTOR = 1;
int VELOCITY_INCREASE_FACTOR_FOR_ACC = 10;
int VELOCITY_DECREASE_FACTOR_INITIAL_CALCULATION = 100;
int POPULATION_SIZE = 100;
int K_INITIAL = 20;
int K_END = 0;
int G_INITIAL = 1.2;
int G_END = 0;
int SEED = 1;
int STARTING_CITY_INDEX = 0;
int PERCENTAGE_2_OPT = 100;
int PERCENTAGE_IGNORED_POPULATION_WHEN_ACC_CALCULATION = 0;
int NR_TEST_RUNS = 1;

int NR_K_MEMBERS_INFLUENCING_SOLUTION = 1;
//0=no; 1=yes
int CALCULATE_MASS_W_MULTIPLICATION_FACTOR = 0;
int MASS_MULTIPLICATION_FACTOR_INDEPENDENT = 2;

int USE_RANDOM_STARTING_POINTS_2_OPT = 0;
int PRINT_PRE_FINAL_2_OPT_VALUES = 0;
int PRINT_POST_FINAL_2_OPT_VALUES = 0;
//pt INITIAL_GENERATION_PROCEDURE: 
//0=random; 
//1=nearest neighbour cu random start point; 
//2=random nearest nighbour din cei mai close nr_points/10 vecini
int INITIAL_GENERATION_PROCEDURE = 0;

//mode: 0=basic-length; 1=length+acc+vel(total); 2: length+acc/vel mean 
int INFO_DISPLAY_MODE = 1;

//closeness metric used: also responsible for the make small move operand
//0 = metric = swap/transposition; small_move = swap/transposition
//1 = metric = nr identical edges in conectivity matrix (how many identical connections 2 solutions have);small move = swap/transposition
int SMALL_MOVE_OPERAND_USED = 0;

//multi_target_movement_in_neighbourhood_space selection
// 0 = all used, best solution used last
// 1 = only NR_K_MEMBERS_INFLUENCING_SOLUTION random k best solutions affects current; does not allow double influences
// 2 = only NR_K_MEMBERS_INFLUENCING_SOLUTION random k best solutions affects current; allows double influences
int MTMNS_INFLUENCER_SELECTION = 0;


ofstream outfile;



///simulates an antenna, radius = effective radius, x,y antenna coordinate, x,y _circuit = point on circuit i should reach
struct point_2d
{
	int index;
	double x;
	double y;
	double radius;
	double x_circuit;
	double y_circuit;

	void set_point_2d(int arg_index, double arg_x, double arg_y, double arg_radius, double arg_x_circuit, double arg_y_circuit)
	{
		index = arg_index;
		x = arg_x;
		y = arg_y;
		radius = arg_radius;
		x_circuit = arg_x_circuit;
		y_circuit = arg_y_circuit;
	}

	void copy(point_2d data_in)
	{
		index = data_in.index;
		x = data_in.x;
		y = data_in.y;
		radius = data_in.radius;
		x_circuit = data_in.x_circuit;
		y_circuit = data_in.y_circuit;
	}


	void print()
	{
		cout << index << " " << x << " " << y << " radius= " << radius << " circuit xy= " << x_circuit << " " << y_circuit << endl;
	}
	


};



double calculate_distance_centers(point_2d a, point_2d b)
{
	return sqrt((a.x - b.x)*(a.x - b.x) + (a.y - b.y)*(a.y - b.y));
}


double calculate_total_distance_centers(vector <point_2d>  circuit)
{
	double result = 0;
	double distance = 0;
	for (unsigned int i = 0; i < circuit.size() - 1; i++)
	{
		distance = calculate_distance_centers(circuit[i], circuit[i + 1]);
		result = result + distance;
	}
	result = result + calculate_distance_centers(circuit[circuit.size() - 1], circuit[0]);
	return result;
}



struct solution
{
	vector<point_2d> points;
	int number_unique;
	double length;
	double fitness;
	double q;
	double mass;
	int velocity;//velocity ca numar de miscari de facut
	vector<int> acceleration;//acceleration ca numar de miscari de facut; acceleration[u] = influenta solutiei u asupra lui this; calc doar de la k_best_solutions

	void initialize_all_0()
	{
		points.clear();
		number_unique = 0;
		length = 0;
		fitness = 0;
		q = 0;
		mass = 0;
		velocity = 0;
		acceleration.clear();
	}


	void copy(solution arg_in)
	{
		number_unique = arg_in.number_unique;
		points.clear();
		points = arg_in.points;
		length = arg_in.length;
		fitness = arg_in.fitness;
		q = arg_in.q;
		mass = arg_in.mass;
		velocity = arg_in.velocity;
		acceleration.clear();
		acceleration = arg_in.acceleration;
	}

	void calculate_fitness()
	{
		length = calculate_total_distance_centers(points);
		fitness = length;
		//add scaling here as needed
	}

	void calculate_q(double fitness_worst, double fitness_best)
	{
		if (fitness_best == fitness_worst)
		{
			q = FLOAT_EQ_CT;
		}
		else
		{
			q = (fitness - fitness_worst) / (fitness_best - fitness_worst);
		}
		
	}

	void calculate_mass(double q_sum )
	{
		mass =  q / q_sum;//original
		//*
		if ( CALCULATE_MASS_W_MULTIPLICATION_FACTOR == 1)
		{
			double a = ceil(points.size() / 50.0);
			double b = (POPULATION_SIZE / 50.0);
			double multiplication_factor = a * b;
			mass = multiplication_factor * q / q_sum; /// XXXX de modificat aici cred ?
		}
		//*/

		mass = MASS_MULTIPLICATION_FACTOR_INDEPENDENT * mass;
	}

	///returns 1 if this is better than other, 0 otherwise
	int check_if_better(solution other)
	{
		if (fitness < other.fitness)
		{
			return 1;
		}
		else
		{
			return 0;
		}
	}


	//generate_solution_nearest_neighbour poate sa bag functia aici. vedem


	void swap_points_by_position_in_points_vector(int first_position, int second_position)
	{
		point_2d temp;
		temp.copy(points[second_position]);
		points[second_position].copy(points[first_position]);
		points[first_position].copy(temp);
	}


	void swap_points_by_index(int first_index, int second_index)
	{
		int first_position=0;
		int second_position=0;

		for (unsigned int i = 0; i < points.size(); i++)
		{
			if (points[i].index == first_index)
			{
				first_position = i;
				break;
			}
		}
		for (unsigned int i = 0; i < points.size(); i++)
		{
			if (points[i].index == second_index)
			{
				second_position = i;
				break;
			}
		}
		swap_points_by_position_in_points_vector(first_position,second_position);
	}


};//end struct solution 









double calculate_distance(point_2d a, point_2d b)
{
	return sqrt((a.x_circuit - b.x_circuit)*(a.x_circuit - b.x_circuit) + (a.y_circuit - b.y_circuit)*(a.y_circuit - b.y_circuit));
}


double calculate_total_distance(vector <point_2d>  circuit)
{
	double result = 0;
	double distance = 0;
	for (unsigned int i = 0; i < circuit.size()-1; i++)
	{
		distance = calculate_distance(circuit[i], circuit[i + 1]);
		result = result + distance;
	}
	result = result + calculate_distance(circuit[circuit.size() - 1], circuit[0]);
	return result;
}


void print_circuit(vector <point_2d>  circuit)
{
	cout << "total circuit length = " << calculate_total_distance(circuit)<<endl;
	cout << "total circuit length center = " << calculate_total_distance_centers(circuit)  << endl;
	for (unsigned int i = 0; i < circuit.size(); i++)
	{
		circuit[i].print();
	}
	cout << endl << endl << endl << endl << endl;
}





bool sort_function_ascending_distance(pair<double, point_2d> a, pair<double, point_2d> b)
{
	return a.first < b.first;
}

bool sort_function_descending_distance(pair<double, point_2d> a, pair<double, point_2d> b)
{
	return a.first > b.first;
}

bool sort_function_ascending_fitness(solution a, solution b)
{
	return a.fitness < b.fitness;
}

bool sort_function_descending_fitness(solution a, solution b)
{
	return a.fitness > b.fitness;
}




solution generate_solution_nearest_neighbour(vector<point_2d> initial_circuit)
{
	int nr_random;
	nr_random = rand() % initial_circuit.size();

	solution solution0;
	solution0.points.push_back(initial_circuit[nr_random]);
	initial_circuit.erase(initial_circuit.begin() + nr_random);

	while (initial_circuit.size() != 0)
	{
		//look for nearest neighbour
		int best_index=0;
		double best_distance = 999999999;
		for (unsigned int i = 0; i < initial_circuit.size(); i++)
		{
			double temp_distance = calculate_distance_centers(solution0.points[solution0.points.size() - 1], initial_circuit[i]);
			if (temp_distance < best_distance)
			{
				best_distance = temp_distance;
				best_index = i;
			}

		}
		//add to solution and delete from initial
		solution0.points.push_back(initial_circuit[best_index]);
		initial_circuit.erase(initial_circuit.begin() + best_index);
	}


	return solution0;
}




solution generate_solution_random(vector<point_2d> initial_circuit)
{
	int nr_random;
	nr_random = rand() % initial_circuit.size();

	solution solution0;
	
	while (initial_circuit.size() != 0)
	{
		nr_random = rand() % initial_circuit.size();
		solution0.points.push_back(initial_circuit[nr_random]);
		initial_circuit.erase(initial_circuit.begin() + nr_random);
	}


	return solution0;
}



solution generate_solution_nearest_neighbour_from_closest(vector<point_2d> initial_circuit, int nr_considered_neighbours)
{
	int nr_random;
	nr_random = rand() % initial_circuit.size();

	solution solution0;
	solution0.points.push_back(initial_circuit[nr_random]);
	initial_circuit.erase(initial_circuit.begin() + nr_random);

	int k = nr_considered_neighbours;
	vector<pair<double,point_2d>> temp;
	
	while (initial_circuit.size() != 0)
	{
		temp.clear();
		for (int i = 0; i < initial_circuit.size(); i++)
		{
			pair<int, point_2d> a;
			a.second.copy(initial_circuit[i]);
			a.first = calculate_distance_centers(solution0.points[solution0.points.size() - 1], initial_circuit[i]);
			temp.push_back(a);
		}

		if (temp.size() < 2)
		{
			//do nothing
		}
		else
		{
			sort(temp.begin(), temp.end(), sort_function_ascending_distance);
		}
		

		nr_random = rand() % ( k< temp.size()?k: temp.size());

		int desired_position_of_selected_point=0;

		for (int j = 0; j < initial_circuit.size(); j++)//search for desired_index_of_selected_point
		{
			if (temp[nr_random].second.index == initial_circuit[j].index)
			{
				desired_position_of_selected_point = j;
				break;
			}
		}

		//add to solution and delete from temp
		solution0.points.push_back(initial_circuit[desired_position_of_selected_point]);
		initial_circuit.erase(initial_circuit.begin() + desired_position_of_selected_point);
	}


	return solution0;
}



vector<solution> generate_initial_population(vector<point_2d> initial_circuit, int population_size)
{
	vector<solution> population; 
	for (int i = 0; i < population_size; i++)
	{
		switch (INITIAL_GENERATION_PROCEDURE)
		{
			case 0: //random generation
			{
				population.push_back(generate_solution_random(initial_circuit));
				break;
			}
			case 1: //nearest neighbour with random start
			{
				population.push_back(generate_solution_nearest_neighbour(initial_circuit));
				break;
			}
			case 2: //nearest neighbour selected randomly from closest neighbours with random start
			{
				population.push_back(generate_solution_nearest_neighbour_from_closest(initial_circuit, initial_circuit.size()/10));
				break;
			}

		}
		population[i].number_unique = i;
	}
	return population;
}





vector<int> invert_permutation(vector<int> arg_in0)
{
	vector<int> result;
	result.resize(arg_in0.size());

	//f: v[i]=>x ;    f-1: v[x]=>i

	for (unsigned int i = 0; i < result.size(); i++)
	{
		result[arg_in0[i]] = i;
	}

	return result;
}

/// result = arg_in0 o arg_in1; 
vector<int> compose_permutations(vector<int> arg_in0, vector<int> arg_in1)
{
	vector<int> result;
	result.resize(arg_in0.size());

	//f: v[i]=>x ;    f-1: v[x]=>i

	for (unsigned int i = 0; i < result.size(); i++)
	{
		result[i] = arg_in1[arg_in0[i]];
	}

	return result;

}



int count_permutation_cycle_decomposition(vector<int> arg_in0)
{
	int result = 0;
	vector<int> visited;
	visited.resize(arg_in0.size(), 0);

	for (unsigned int i = 0; i < visited.size(); i++)
	{
		if (visited[i] == 0)//if not yet visited
		{
			int j = i;
			result = result + 1;
			while (visited[j] != 1)//not reached end of cycle (visited the same point twice), make another move
			{
				visited[j] = 1;
				j = arg_in0[j];
			}
		}

	}

	return result;
}



int count_minimum_number_of_transpositions_between_permutations(vector<int> permutation0, vector<int> permutation1)
{
	int result = 0;
	result = permutation0.size() - count_permutation_cycle_decomposition(compose_permutations(invert_permutation(permutation1), permutation0));


	return result;
}







vector<vector<int>> make_matrix_from_route(vector<int> route)
{
	vector<vector<int>> result;
	//fac nxn matrice goala
	vector<int> temp(route.size(), 0);

	for (int i = 0; i < route.size(); i++)
	{
		result.push_back(temp);
	}

	// pun 1 unde trebuie
	for (int i = 0; i < route.size(); i++)
	{
		int next_point_index;
		int current_point_index;
		int previous_point_index;

		current_point_index = route[i];
		next_point_index = route[(i + 1) % route.size()];
		if (i == 0)
		{
			previous_point_index = route[route.size() - 1];
		}
		else
		{
			previous_point_index = route[(i - 1)];
		}
		result[current_point_index][previous_point_index] = 1;
		result[current_point_index][next_point_index] = 1;
	}

	return result;
}



vector<vector<int>> make_matrix_from_route(vector<point_2d> arg_route)
{
	vector<int> route;

	for (int i = 0; i < arg_route.size(); i++)
	{
		route.push_back(arg_route[i].index);
	}

	return make_matrix_from_route(route);
}



vector<vector<int>> sub_matrix(vector<vector<int>> a, vector<vector<int>> b)
{
	vector<vector<int>> result;

	//check sizes // lene sa implementez


	//actual add
	vector<int> temp(a.size(), 0);

	for (int i = 0; i < a.size(); i++)
	{
		for (int j = 0; j < a[i].size(); j++)
		{
			temp[j] = a[i][j] - b[i][j];
		}
		result.push_back(temp);
	}



	return result;
}


int count_nr_of(int counted_element, vector<vector<int>> matrix)
{
	int result = 0; 
	for (int i = 0; i < matrix.size(); i++)
	{
		for (int j = 0; j < matrix[i].size(); j++)
		{
			if (matrix[i][j] == counted_element)
			{
				result = result + 1;
			}
		}
	}
	return result;
}



int calculate_smns_distance_min(solution start_solution, solution destination_solution)
{
	int result = 0;

	switch (SMALL_MOVE_OPERAND_USED)
	{
		case(0):
		{
			vector<int> initial_permutation;
			vector<int> final_permutation;

			for (unsigned int i = 0; i < start_solution.points.size(); i++)
			{
				initial_permutation.push_back(start_solution.points[i].index);
				final_permutation.push_back(destination_solution.points[i].index);
			}

			result = count_minimum_number_of_transpositions_between_permutations(initial_permutation, final_permutation);
			break;
		}
		case(1):
		{
			//bigger distance, worse solution
			
			// idea: make conectivity matrix, sub them, coutn nr of conections that appear in one and not the other
			//*
			vector<vector<int>> a;
			vector<vector<int>> b;
			vector<vector<int>> c;
			a = make_matrix_from_route(start_solution.points);
			b = make_matrix_from_route(destination_solution.points);
			c = sub_matrix(a, b);
			result = count_nr_of(1, c) / 2;

			break;
		}


	}

	

	return result;
}




double calculate_distance_between_solutions(solution start_solution, solution end_solution)
{
	double result = 0;

	result = calculate_smns_distance_min(start_solution, end_solution);

	return result;
}


///initial velocity is generated randomly
void generate_initial_velocity(vector<solution> & solutions)
{
	//calculate max distance between any two solutions
	double max_distance;
	max_distance = calculate_distance_between_solutions(solutions[0], solutions[0]);

	for (unsigned int i = 0; i < solutions.size(); i++)
	{
		for (unsigned int j = 0; j < solutions.size(); j++)
		{
			double temp_distance= 0;
			temp_distance = calculate_distance_between_solutions(solutions[i], solutions[j]);
			if (max_distance < temp_distance)
			{
				max_distance = temp_distance;
			}
		}
	}
	
	//actual velocity calculation
	for (unsigned int i = 0; i < solutions.size(); i++)
	{
		solutions[i].velocity = 0;
		double nr_random=0;
		if (max_distance != 0)
		{
			nr_random = rand() % int(max_distance);
			solutions[i].velocity = int(floor(nr_random));
			solutions[i].velocity = solutions[i].velocity / VELOCITY_DECREASE_FACTOR_INITIAL_CALCULATION;
		}
		else
		{
			nr_random = 0;
			solutions[i].velocity = int(floor(nr_random));
			solutions[i].velocity = solutions[i].velocity / VELOCITY_DECREASE_FACTOR_INITIAL_CALCULATION;
		}
		
	}
	
}





void calculate_fitness(vector<solution> & population)
{
	for (unsigned int i = 0; i < population.size(); i++)
	{
		population[i].calculate_fitness();
	}
}

void calculate_mass(vector<solution> & population)
{
	solution fitness_best;
	solution fitness_worst;
	double q_sum = 0;

	fitness_best.fitness = 999999999;
	fitness_worst.fitness = 0;
	for (unsigned int i = 0; i < population.size(); i++)
	{
		
		if (fitness_best.check_if_better(population[i]) == 0)//if best is not better than current
		{
			fitness_best.fitness = population[i].fitness;
		}
		if (fitness_worst.check_if_better(population[i]) == 1)//if worst is better than current
		{
			fitness_worst.fitness = population[i].fitness;
		}
	}

	for (unsigned int i = 0; i < population.size(); i++)
	{
		population[i].calculate_q(fitness_worst.fitness, fitness_best.fitness);
		q_sum = q_sum + population[i].q;
	}

	for (unsigned int i = 0; i < population.size(); i++)
	{
		population[i].calculate_mass(q_sum);
	}


}



int stop_condition(int nr_iterations)
{
	if (MAX_NR_ITERATIONS > nr_iterations)
	{
		return 0;
	}
	else
	{
		return 1;
	}
}

///implemented linear decreasing
double calculate_g(double g_initial, double g_end, int nr_iterations)
{
	double result=0;
	double step = (g_initial - g_end) / MAX_NR_ITERATIONS;
	result = (MAX_NR_ITERATIONS-nr_iterations) * step;
	return result;
}

///implemented linear decreasing
int calculate_k(double k_initial, int nr_iterations)
{
	int result;
	double step = (k_initial- K_END) / MAX_NR_ITERATIONS;
	result = int(ceil(k_initial - nr_iterations * step));
	return result;
}




vector<solution> find_k_best(vector<solution> population, int k)
{
	vector<solution> result;
	if (population.size() < 2)
	{
		//do nothing
	}
	else
	{
		sort(population.begin(), population.end(), sort_function_ascending_fitness);//ascending fitness. lower is better (fitness==tour distance)
		
	}
	vector<solution>::iterator it = result.begin();
	result.insert(it, population.begin(), population.begin() + k);

	return result;
}





int calculate_smns_distance_max(solution start_solution, solution end_solution)
{
	//if possible. nu e chiar corect dar asta e // XXXX poate ask
	return start_solution.points.size()-1;
}


double normalize_in_interval(double interval_start, double interval_end, double value,solution start_solution, solution end_solution)
{
	return interval_start + (value / (2.0 * calculate_smns_distance_max(start_solution, end_solution)));
}


double calculate_acceleration_from_solution(solution influenced_solution, solution  influencing_solution, double g)
{	
	// acc = Fij/Mi => direct mai jos
	
	//double Fij = ((double)(rand() % 1000) / 1000) * g * ((influenced_solution.mass * influencing_solution.mass) / (normalize_in_interval(0.5,1,Rij, influenced_solution, influencing_solution))) * Rij;
	double acc = 0;
	int Rij = calculate_smns_distance_min(influenced_solution, influencing_solution);
	double rand_number = ((double)(rand() % 1000)) / 1000.0;
	acc = rand_number * g * Rij *
		(influencing_solution.mass / (normalize_in_interval(0.5, 1, Rij, influenced_solution, influencing_solution))) ;
	//double acc_multiplication_factor = influenced_solution.points.size()*POPULATION_SIZE/;

	return acc;
}



void calculate_acceleration(solution & population_member, vector<solution>  k_best_solutions, double g)
{
	population_member.acceleration.clear();
	for (unsigned int i = 0; i < k_best_solutions.size(); i++)
	{
		population_member.acceleration.push_back(int(calculate_acceleration_from_solution(population_member,k_best_solutions[i],g)));
	}


}



void calculate_acceleration_k_best(vector<solution> & population, vector<solution> k_best_solutions, double g)
{
	for (unsigned int i = 0; i < population.size(); i++)
	{
		calculate_acceleration(population[i], k_best_solutions, g);

	}


}




solution make_small_move(solution starting_solution, solution target_solution)
{

	solution result;
	int distance_between_start_target_solutions = int(calculate_distance_between_solutions(starting_solution, target_solution));
	

	switch (SMALL_MOVE_OPERAND_USED)
	{
		case(0):
		{
			// implementation 0: swap operation between 2 points in tour
			// problem: can freeze: ex: target= 1 2 3 4 5; start = 1 4 2 3 5; guess its ok ,reached local minimum.
			// nu caut steps care sa produca longest path neaparat i guess. 
			//*/
			int rand_nr_1 = rand() % starting_solution.points.size();
			int rand_nr_2 = rand() % starting_solution.points.size();

			int rand_nr_limit1 = rand_nr_1 + starting_solution.points.size();
			int rand_nr_limit2 = rand_nr_2 + starting_solution.points.size();

			for (unsigned int i = rand_nr_1; i < rand_nr_limit1; i++)
			{
				for (unsigned int j = rand_nr_2; j < rand_nr_limit2; j++)
				{
					result.copy(starting_solution);
					result.swap_points_by_index(i%starting_solution.points.size(), j%starting_solution.points.size());
					if (calculate_distance_between_solutions(result, target_solution) < distance_between_start_target_solutions)
					{
						return result;
					}
				}
			}
			//*/
			break;
		}//end case 0
		case(1):
		{
			// implementation 1: swap operation between 2 points in tour + metric nr of different connections
			//*/
			int rand_nr_1 = rand() % starting_solution.points.size();
			int rand_nr_2 = rand() % starting_solution.points.size();

			int rand_nr_limit1 = rand_nr_1 + starting_solution.points.size();
			int rand_nr_limit2 = rand_nr_2 + starting_solution.points.size();

			for (unsigned int i = rand_nr_1; i < rand_nr_limit1; i++)
			{
				for (unsigned int j = rand_nr_2; j < rand_nr_limit2; j++)
				{
					result.copy(starting_solution);
					result.swap_points_by_index(i%starting_solution.points.size(), j%starting_solution.points.size());
					if (calculate_distance_between_solutions(result, target_solution) < distance_between_start_target_solutions)
					{
						return result;
					}
				}
			}
			//*/
			break;
		}//end case 1
	}

	return result;
}


solution make_simple_movement_in_neighbourhood_space(solution starting_solution, solution target_solution, int movement_length)
{
	solution result;
	result.copy(starting_solution);
	for (int i = 0; i < movement_length; i++)
	{
		result = make_small_move(result,target_solution);
	}
	return result;
}



solution solve_multi_target_movement_in_neighbourhood_space(solution starting_solution, vector<solution> k_best_solutions)
{//nr de target solutions e dat de k_best_solutions.size() si movement lengths e dat de acceleratie care e incorporata in solutie "starting_solution.acceleration"
	//k_best_solutions already sorted descending dupa fitness. o sa iau worst solution makes first move
	solution result;
	result.copy(starting_solution);



	switch(MTMNS_INFLUENCER_SELECTION)
	{
		case(0)://all used, best solution used last
		{
			for (int i = k_best_solutions.size() - 1; i >= 0; i--)
			{
				result = make_simple_movement_in_neighbourhood_space(result, k_best_solutions[i], result.acceleration[i]);
			}
			break;
		}
		case(1)://only NR_K_MEMBERS_INFLUENCING_SOLUTION random k best solutions affects current; does not allow double influences
		{
			
			for (int i = 0; i < NR_K_MEMBERS_INFLUENCING_SOLUTION; i++)
			{
				if (k_best_solutions.size() == 0)
				{
					return result;
				}
				else
				{
					int nr_random = rand() % k_best_solutions.size();
					result = make_simple_movement_in_neighbourhood_space(result, k_best_solutions[nr_random], result.acceleration[nr_random]);
					k_best_solutions.erase(k_best_solutions.begin() + nr_random);
				}
			}
			break;
		}
		case(2)://only NR_K_MEMBERS_INFLUENCING_SOLUTION random k best solutions affects current; allows double influences
		{
			for (int i = 0; (i < NR_K_MEMBERS_INFLUENCING_SOLUTION)&&(i<k_best_solutions.size()); i++)
			{
				int nr_random = rand() % k_best_solutions.size();
				result = make_simple_movement_in_neighbourhood_space(result, k_best_solutions[nr_random], result.acceleration[nr_random]);
			}
			break;
		}
		default:
		{
			break;
		}
	}


	
	return result;
}



solution two_opt_swap(solution starting_solution, int first_limit, int second_limit)
{
	solution result;
	result.copy(starting_solution);
	result.points.clear();

	for (int i = 0; i < first_limit; i++)
	{
		result.points.push_back(starting_solution.points[i]);
	}
	for (int i = second_limit-1; i >= first_limit; i--)
	{
		result.points.push_back(starting_solution.points[i]);
	}
	for (unsigned int i = second_limit; i < starting_solution.points.size(); i++)
	{
		result.points.push_back(starting_solution.points[i]);
	}



	return result;
}



solution solve_modified_2_opt(solution starting_solution,int iteration)
{
	double random_nr = (rand() % 1000) / 1000.0;
	int movement_length;

	if (iteration % EVERY_X_ITERATIONS_DO_Y_2OPT_MOVEMENTS.first == 0)
	{
		movement_length = EVERY_X_ITERATIONS_DO_Y_2OPT_MOVEMENTS.second + int(random_nr * starting_solution.velocity);
	}
	else
	{
		movement_length = + int(random_nr * starting_solution.velocity);
	}

	int movements_done = 0;

	if (movement_length == 0)
	{
		return starting_solution;
	}
	solution result;
	result.copy(starting_solution);

	int n = starting_solution.points.size();
	int rand_i;
	if (USE_RANDOM_STARTING_POINTS_2_OPT == 1)
	{
		rand_i = rand() % n - 1;
	}
	else
	{
		rand_i = 0;
	}

algorithm_start:
	double best_distance = calculate_total_distance_centers(result.points);

	for (unsigned int i = 0; i < starting_solution.points.size()-1; i++)
	{
		int rand_k;
		if (USE_RANDOM_STARTING_POINTS_2_OPT == 1)
		{
			rand_k = rand() % n - 1;
		}
		else
		{
			rand_k = 0;
		}
		for (unsigned int k = i + 1; k < starting_solution.points.size(); k++)
		{
			solution temp = two_opt_swap(result, (i+rand_i) % (n-1) , (k+rand_k) % n);
			double temp_distance = calculate_total_distance_centers(temp.points);
			if (temp_distance < best_distance)
			{
				result.copy(temp);
				best_distance = temp_distance;
				movements_done = movements_done + 1;
				if (movements_done >= movement_length)
				{
					return result;
				}
				goto algorithm_start;
			}
		}
	}

	return result;
}


//complete 2 opt solver until no improvements can be made
solution solve_2_opt(solution starting_solution)
{
	solution result;
	result.copy(starting_solution);

	int n = starting_solution.points.size();
	int rand_i;
	if (USE_RANDOM_STARTING_POINTS_2_OPT == 1)
	{
		rand_i = rand() % n - 1;
	}
	else
	{
		rand_i = 0;
	}

algorithm_start:
	double best_distance = calculate_total_distance_centers(result.points);

	for (unsigned int i = 0; i < starting_solution.points.size() - 1; i++)
	{
		int rand_k;
		if (USE_RANDOM_STARTING_POINTS_2_OPT == 1)
		{
			rand_k = rand() % n - 1;
		}
		else
		{
			rand_k = 0;
		}
		
		for (unsigned int k = i + 1; k < starting_solution.points.size(); k++)
		{
			solution temp = two_opt_swap(result, (i + rand_i) % (n - 1), (k + rand_k) % n);
			double temp_distance = calculate_total_distance_centers(temp.points);
			if (temp_distance < best_distance)
			{
				result.copy(temp);
				best_distance = temp_distance;
				goto algorithm_start;
			}
		}
	}

	return result;
}




void calculate_velocity(vector<solution> & population)
{
	//paper described version, adapted to all accelerations
	//*
	for (unsigned int i = 0; i < population.size(); i++)
	{
		double acceleration_mean = 0;
		for (unsigned int j = 0; j < population[i].acceleration.size(); j++)
		{
			acceleration_mean = acceleration_mean + population[i].acceleration[j];
		}
		acceleration_mean = acceleration_mean / population[i].acceleration.size();
		double random_number = rand();
		random_number = ((int(random_number) % 1000) / 1000.0);
		population[i].velocity = int(random_number * population[i].velocity + acceleration_mean* VELOCITY_INCREASE_FACTOR_FOR_ACC);
		population[i].velocity = population[i].velocity*1.0 / VELOCITY_DECREASE_FACTOR;
		
	}
	//*/


}


vector<point_2d> shift_points_right(vector<point_2d> what_im_shifting, int how_much)
{
	vector<point_2d> result;
	int n = what_im_shifting.size();
	for (int i = 0; i < n; i++)
	{
		result.push_back(what_im_shifting[(n- how_much+i)%n]);
	}

	return result;
}


int find_position_of_index(solution arg_solution, int index_to_find)
{
	unsigned int starting_city_index_position = 0;
	for (starting_city_index_position = 0; starting_city_index_position < arg_solution.points.size(); starting_city_index_position++)
	{
		if (index_to_find == arg_solution.points[starting_city_index_position].index)
		{
			return starting_city_index_position;
		}
	}
	return starting_city_index_position;
}


vector<solution> shift_population(vector<solution> population, int starting_city_index)
{
	vector<solution> result= population;
	for (unsigned int i = 0; i < population.size(); i++)
	{
		result[i].points.clear();
		int temp = 0;
		temp = find_position_of_index(population[i], starting_city_index);//find position of starting_city_index inside each solution
		result[i].points = shift_points_right(population[i].points,(population[i].points.size() - temp));//shift all by that amount so that starting_city_index is first
	}
	return result;
}

solution find_best(vector<solution> population)
{
	solution result;
	result.copy(population[0]);
	for (unsigned int i = 0; i < population.size(); i++)
	{
		if (result.check_if_better(population[i]) == 0)
		{
			result.copy(population[i]);
		}
	}
	return result;
}



void print_best(vector< solution> population)
{
	cout << "best distance = " << calculate_total_distance_centers(find_best(population).points);
}



void display_infos(vector <solution>population,int nr_iterations,int mode)
{
	double total_length_mean = 0;
	double mean_acceleration = 0;
	double mean_velocity = 0; 
	for (unsigned int i = 0; i < population.size(); i++)
	{
		total_length_mean += calculate_total_distance_centers(population[i].points);
		for (int j = 0; j < population[i].acceleration.size(); j++)
		{
			mean_acceleration = mean_acceleration + population[i].acceleration[j];
		}
		mean_velocity = mean_velocity + population[i].velocity;

	}
	total_length_mean = total_length_mean / population.size();
	

	//std::cout << fixed;
	//std::cout << setprecision(4);
	switch(mode)
	{
		case 0:
		{
			cout << "iteration: " << nr_iterations << " of " << MAX_NR_ITERATIONS << " total_length_mean = " << total_length_mean << " ";
			print_best(population);
			cout << endl;
			break;
		}
		case 1:
		{
			cout << "iteration: " << nr_iterations << " of " << MAX_NR_ITERATIONS << " total_length_mean = " << total_length_mean << " ";
			print_best(population);
			cout << endl;
			cout << "total acceleration = " << mean_acceleration << " total velocity = " << mean_velocity;
			break;
		}
		case 2:
		{
			cout << "iteration: " << nr_iterations << " of " << MAX_NR_ITERATIONS << " total_length_mean = " << total_length_mean << " ";
			print_best(population);
			cout << endl;
			mean_acceleration = mean_acceleration / population[0].acceleration.size();
			mean_acceleration = mean_acceleration / population.size();
			mean_velocity = mean_velocity / population.size();
			cout << "mean acceleration = " << mean_acceleration << " mean velocity = " << mean_velocity;
			break;
		}
	}

	cout << endl << endl;
}


/// initial circuit = circuit as read from file
/// s = population size (nr searcher agents)
/// k/g iniital - k for k_best selection, g for gravitational constant. both decrease over time
solution solve_dgsa_tsp(vector<point_2d> initial_circuit, int population_size,double k_initial, double g_initial, double g_end)
{
	double g = g_initial;
	double k = k_initial;
	vector <solution> k_best_solutions;
	solution result;
	//initialization phase
	vector<solution> population = generate_initial_population(initial_circuit, population_size);
	population = shift_population(population, STARTING_CITY_INDEX);
	calculate_fitness(population);
	calculate_mass(population);
	generate_initial_velocity(population);

	//main phase
	int nr_iterations = 0;
	display_infos(population, nr_iterations,INFO_DISPLAY_MODE);
	while (stop_condition(nr_iterations) == 0)
	{
		//update g k k_best
		g = calculate_g(g_initial, g_end, nr_iterations);
		k = calculate_k(k_initial,nr_iterations);
		
		if (population.size() < 2)
		{
			//do nothing.
		}
		else
		{
			sort(population.begin(), population.end(), sort_function_ascending_fitness);//ascending fitness. lower is better (fitness==tour distance)
		}
		
		k_best_solutions = find_k_best(population, int(k));

		//dependent movement operator
			//Calculate lij (acceleratia) generated by each agent j from Kbest into each agent i
			//mtmns
		calculate_acceleration_k_best(population, k_best_solutions,g);
		//display_infos(population, nr_iterations, INFO_DISPLAY_MODE);
		int x = int(1.0 * population.size() * (PERCENTAGE_IGNORED_POPULATION_WHEN_ACC_CALCULATION / 100.0));
		for (unsigned int i = x; i < population.size(); i++)
		{
			population[i].copy(solve_multi_target_movement_in_neighbourhood_space(population[i],k_best_solutions));
		}
		
		//independent movement operator
		calculate_velocity(population);//asta sa il mai fac o data 
			//the next velocity of the ith agent is calculated as a fraction of its current velocity added to its acceleration
		for (unsigned int i = 0; i < population.size(); i++)
		{
			population[i].copy(solve_modified_2_opt(population[i], nr_iterations));
		}

		population = shift_population(population, STARTING_CITY_INDEX);// so that all routes have starting city as first in vector
		//evaluate new fitness
		calculate_fitness(population);
		//calculate new mass
		calculate_mass(population);

		nr_iterations = nr_iterations + 1;
		display_infos(population, nr_iterations,INFO_DISPLAY_MODE);
	}

	if (population.size() < 2)
	{
		//do nothing
	}
	else
	{
		sort(population.begin(), population.end(), sort_function_ascending_fitness);
	}

	vector<solution> population_pre_final_2opt = population;
	if (PRINT_PRE_FINAL_2_OPT_VALUES == 1)
	{
		//print mean and best pre 2opt to file
		double total_length_mean = 0;
		for (unsigned int i = 0; i < population.size(); i++)//calculate mean
		{
			total_length_mean = total_length_mean + calculate_total_distance_centers(population[i].points);
		}
		total_length_mean = total_length_mean / population.size();

		outfile << "pre final 2-opt:";
		outfile << " best_length = " << calculate_total_distance(population[0].points);
		outfile << " best_length _centers = " << calculate_total_distance_centers(population[0].points);
		outfile << " total_length_mean = " << total_length_mean << endl;
		cout << "pre final 2-opt:";
		cout << " best_length = " << calculate_total_distance(population[0].points);
		cout << " best_length _centers = " << calculate_total_distance_centers(population[0].points);
		cout << " total_length_mean = " << total_length_mean << endl;
	}
	
	//final 2-opt for population
	cout << "final 2-opt: ";
	int n = (PERCENTAGE_2_OPT /100.0) * population.size();
	for (unsigned int i = 0; i < n; i++)
	{
		population[i].copy(solve_2_opt(population[i]));
		cout << i << " ";
	}
	cout << endl;
	calculate_fitness(population);

	if (population.size() < 2)
	{
		//do nothing
	}
	else
	{
		sort(population.begin(), population.end(), sort_function_ascending_fitness);
	}
	vector<solution> population_post_final_2opt = population;

	if (PRINT_POST_FINAL_2_OPT_VALUES == 1)
	{
		//print mean and best pre 2opt to file
		double total_length_mean = 0;
		for (unsigned int i = 0; i < population.size(); i++)//calculate mean
		{
			total_length_mean = total_length_mean + calculate_total_distance_centers(population[i].points);
		}
		total_length_mean = total_length_mean / population.size();

		outfile << "post final 2-opt:";
		outfile << " best_length = " << calculate_total_distance(population[0].points);
		outfile << " best_length _centers = " << calculate_total_distance_centers(population[0].points);
		outfile << " total_length_mean = " << total_length_mean << endl;
	}



	//output stuff
	display_infos(population, nr_iterations, INFO_DISPLAY_MODE);
	/*
	result.copy(population[0]);
	for (unsigned int i = 0; i < population.size(); i++)
	{
		if (result.check_if_better(population[i]) == 0)
		{
			result.copy(population[i]);
		}
	}
	*/
	result.copy(population[0]);
	return result;
}




string test_settings_to_string()
{
	string result = "";
	result = "FLOAT_EQ_CT = " + to_string(FLOAT_EQ_CT) + "\n" +
		"STARTING_CITY_INDEX = " + to_string(STARTING_CITY_INDEX) + "\n" +
		"SEED = " + to_string(SEED) + "\n" +
		"NR_TEST_RUNS = " + to_string(NR_TEST_RUNS) + "\n" +
		"MAX_NR_ITERATIONS = " + to_string(MAX_NR_ITERATIONS) + "\n" +
		"POPULATION_SIZE = " + to_string(POPULATION_SIZE) + "\n" +
		"PERCENTAGE_2_OPT = " + to_string(PERCENTAGE_2_OPT) + "\n" +
		"PERCENTAGE_IGNORED_POPULATION_WHEN_ACC_CALCULATION = " + to_string(PERCENTAGE_IGNORED_POPULATION_WHEN_ACC_CALCULATION) + "\n" +
		"K_INITIAL = " + to_string(K_INITIAL) + "\n" +
		"G_INITIAL = " + to_string(G_INITIAL) + "\n" +
		"G_END = " + to_string(G_END) + "\n" +
		"VELOCITY_DECREASE_FACTOR = " + to_string(VELOCITY_DECREASE_FACTOR) + "\n" +
		"VELOCITY_INCREASE_FACTOR_FOR_ACC = " + to_string(VELOCITY_INCREASE_FACTOR_FOR_ACC) + "\n" +
		"VELOCITY_DECREASE_FACTOR_INITIAL_CALCULATION = " + to_string(VELOCITY_DECREASE_FACTOR_INITIAL_CALCULATION) + "\n" +
		"NR_K_MEMBERS_INFLUENCING_SOLUTION = " + to_string(NR_K_MEMBERS_INFLUENCING_SOLUTION) + "\n" +
		"MTMNS_INFLUENCER_SELECTION = " + to_string(MTMNS_INFLUENCER_SELECTION) + "\n" +
		"USE_RANDOM_STARTING_POINTS_2_OPT = " + to_string(USE_RANDOM_STARTING_POINTS_2_OPT) + "\n" +
		"CALCULATE_MASS_W_MULTIPLICATION_FACTOR = " + to_string(CALCULATE_MASS_W_MULTIPLICATION_FACTOR) + "\n" +
		"MASS_MULTIPLICATION_FACTOR_INDEPENDENT = " + to_string(MASS_MULTIPLICATION_FACTOR_INDEPENDENT) + "\n" +
		"VELOCITY_DECREASE_FACTOR_INITIAL_CALCULATION = " + to_string(VELOCITY_DECREASE_FACTOR_INITIAL_CALCULATION) + "\n" +
		"PRINT_PRE_FINAL_2_OPT_VALUES = " + to_string(PRINT_PRE_FINAL_2_OPT_VALUES) + "\n" +
		"PRINT_POST_FINAL_2_OPT_VALUES = " + to_string(PRINT_POST_FINAL_2_OPT_VALUES) + "\n" +
		"INITIAL_GENERATION_PROCEDURE = " + to_string(INITIAL_GENERATION_PROCEDURE) + "\n" +
		"INFO_DISPLAY_MODE = " + to_string(INFO_DISPLAY_MODE) + "\n" +
		"SMALL_MOVE_OPERAND_USED = " + to_string(SMALL_MOVE_OPERAND_USED) + "\n";

		return result;
}











void run_test_suite(string FILE_PATH, string FILE_NAME, int unique_id)
{


	vector<point_2d> initial_points;

	ifstream inFile;

	inFile.open(FILE_PATH + FILE_NAME);

	if (!inFile) {
		cerr << "Unable to open file datafile.txt";
		exit(1);   // call system to stop
	}

	string temp;
	vector <point_2d> points;
	point_2d temp_point;

	inFile >> temp; //index 
	while (temp != "EOF")
	{
		temp_point.index = stoi(temp);
		inFile >> temp;//x
		temp_point.x = stoi(temp);
		inFile >> temp;//y
		temp_point.y = stoi(temp);
		initial_points.push_back(temp_point);
		inFile >> temp; //index 
	}


	outfile.open(FILE_PATH +"results\\"+ FILE_NAME + to_string(unique_id) + "_results.txt");

	cout << "STARTING RUN for file " << FILE_NAME  << " UNIQUE ID = "<<unique_id<< endl << endl << endl;
	outfile<< "STARTING RUN for file " << FILE_NAME << " UNIQUE ID = " << unique_id << endl << endl << endl;

	cout << test_settings_to_string() << endl << endl;
	outfile << test_settings_to_string() << endl << endl;

	vector<double> results;
	for (int i = 0; i < NR_TEST_RUNS; i++)
	{
		cout << endl << endl << endl << "RUN " << i << " OF " << NR_TEST_RUNS - 1 << endl;
		outfile << endl << endl << endl << "RUN " << i << " OF " << NR_TEST_RUNS - 1 << endl;
		//dgsa
		solution solution0;
		solution0 = solve_dgsa_tsp(initial_points, POPULATION_SIZE, K_INITIAL, G_INITIAL, G_END);
		outfile << "final solution: ";
		outfile << "best_length = " << calculate_total_distance(solution0.points);
		outfile << " best_length_center = " << calculate_total_distance_centers(solution0.points) << endl << endl;
		print_circuit(solution0.points);
		results.push_back(calculate_total_distance_centers(solution0.points));
	}
	double mean = 0;
	for (int i = 0; i < results.size(); i++)
	{
		mean = mean + results[i];
	}
	mean = mean / results.size();

	map<double, int> histogram_results;
	//calculate + print histogram
	cout << "PRINTING RESULTS HISTOGRAM\n";
	outfile << "PRINTING RESULTS HISTOGRAM\n";
	for (int i = 0; i < results.size(); i++)
	{
		histogram_results[int(floor(results[i]))] += 1;
	}
	int  ok = 0;
	int best = 0;
	vector<double> results_percentage_histogram;//v[0]=0%err fata de best;v[1]=under 1%; ca procente
	for (int k = 0; k < 101; k++)
	{
		results_percentage_histogram.push_back(0);
	}
	for (auto elem : histogram_results)
	{//aici am ramas sa verific de ce nu imi face histograma bine
		if (ok == 0)
		{
			best = int(floor(elem.first));
			ok = 1;
		}
		//calculate error;
		double error = 0;
		error = (abs(elem.first - best) / best) * 100;//relative error
		if (error > 100)
		{
			results_percentage_histogram[100] += elem.second;
		}
		else
		{
			results_percentage_histogram[ceil(error)] += elem.second;
		}
		
		

		cout << elem.first << " : " << elem.second << "\n";
		outfile << elem.first << " : " << elem.second << "\n";
	}

	//between  0 an 1 % difference compared to best, i find results_percentage_histogram % results
	cout << "RESULTS_PERCENTAGE_DIFFERENCE_HISTOGRAM" << endl;
	outfile << "RESULTS_PERCENTAGE_DIFFERENCE_HISTOGRAM" << endl;
	for (int k = 0; k < 101; k++)
	{
		results_percentage_histogram[k] = (results_percentage_histogram[k] / results.size()) * 100;
		cout << "(" << k-1 <<":" <<k<<"] % : " <<  results_percentage_histogram[k] << " %" <<endl;
		outfile << "(" << k-1 << ":" << k << "] % : " << results_percentage_histogram[k] << " %" << endl;
	}


	//final prints
	cout << "BEST LENGTH CENTER MEAN OVER " << NR_TEST_RUNS << " TESTS IS " << mean << endl;
	outfile << "BEST LENGTH CENTER MEAN OVER " << NR_TEST_RUNS << " TESTS IS " << mean << endl;

	cout << "FINISHED RUN" << endl << endl << endl;
	outfile << "FINISHED RUN" << endl << endl << endl;
	outfile.close();

}



void proces_input_arguments(int argc, char *argv[])
{
	switch (argc)
	{
		case(1):
		{
			FLOAT_EQ_CT = 0.00001;
			STARTING_CITY_INDEX = 0;

			SEED = 1;
			NR_TEST_RUNS = 1;
			MAX_NR_ITERATIONS = 500;
			POPULATION_SIZE = 100;

			PERCENTAGE_2_OPT = 100;//does 2opt over best part of population
			PERCENTAGE_IGNORED_POPULATION_WHEN_ACC_CALCULATION = 0;//best part of population is not changing

			K_INITIAL = 10;
			K_END = 0;
			G_INITIAL = 1.2;
			G_END = 0;
			VELOCITY_DECREASE_FACTOR = 1;
			EVERY_X_ITERATIONS_DO_Y_2OPT_MOVEMENTS.first = 10;
			EVERY_X_ITERATIONS_DO_Y_2OPT_MOVEMENTS.second = 10;//minimum of 2opt movements
			VELOCITY_INCREASE_FACTOR_FOR_ACC = 10;//usually 10
			VELOCITY_DECREASE_FACTOR_INITIAL_CALCULATION = 3;

			NR_K_MEMBERS_INFLUENCING_SOLUTION = K_INITIAL;//max nr possible ; usually K_INITIAL

			//0=no; 1=yes
			USE_RANDOM_STARTING_POINTS_2_OPT = 0;
			CALCULATE_MASS_W_MULTIPLICATION_FACTOR = 0;
			MASS_MULTIPLICATION_FACTOR_INDEPENDENT = 2;//usually 2
			PRINT_PRE_FINAL_2_OPT_VALUES = 1;
			PRINT_POST_FINAL_2_OPT_VALUES = 1;
			//pt INITIAL_GENERATION_PROCEDURE: 
			//0=random; 
			//1=nearest neighbour cu random start point; 
			//2=random nearest nighbour din cei mai close nr_points/10 vecini
			INITIAL_GENERATION_PROCEDURE = 0;

			//pt info display mode: 0=basic-length; 1=length+acc+vel(total); 2: length+acc/vel mean 
			INFO_DISPLAY_MODE = 1;

			//closeness metric used: also responsible for the make small move operand
			//0 = metric = swap/transposition; small_move = swap/transposition
			//1 = metric = nr identical edges in conectivity matrix (how many identical connections 2 solutions have);small move = swap/transposition
			SMALL_MOVE_OPERAND_USED = 0;

			//multi_target_movement_in_neighbourhood_space selection
			// 0 = all used, best solution used last
			// 1 = only NR_K_MEMBERS_INFLUENCING_SOLUTION random k best solutions affects current; does not allow double influences
			// 2 = only NR_K_MEMBERS_INFLUENCING_SOLUTION random k best solutions affects current; allows double influences
			MTMNS_INFLUENCER_SELECTION = 0;

			break;
		}
		case(49):
		{
			FLOAT_EQ_CT = atof(argv[2]);
			STARTING_CITY_INDEX = atoi(argv[4]);
			SEED = atoi(argv[6]);
			NR_TEST_RUNS = atoi(argv[8]);
			MAX_NR_ITERATIONS = atoi(argv[10]);
			POPULATION_SIZE = atoi(argv[12]);
			PERCENTAGE_2_OPT = atof(argv[14]);
			PERCENTAGE_IGNORED_POPULATION_WHEN_ACC_CALCULATION = atof(argv[16]);
			K_INITIAL = atof(argv[18]);
			G_INITIAL = atof(argv[20]);
			G_END = atof(argv[22]);
			VELOCITY_DECREASE_FACTOR = atof(argv[24]);
			VELOCITY_INCREASE_FACTOR_FOR_ACC = atof(argv[26]);
			VELOCITY_DECREASE_FACTOR_INITIAL_CALCULATION = atof(argv[28]);
			NR_K_MEMBERS_INFLUENCING_SOLUTION = atoi(argv[30]);
			USE_RANDOM_STARTING_POINTS_2_OPT = atoi(argv[32]);
			CALCULATE_MASS_W_MULTIPLICATION_FACTOR = atoi(argv[34]);
			MASS_MULTIPLICATION_FACTOR_INDEPENDENT = atof(argv[36]);
			PRINT_PRE_FINAL_2_OPT_VALUES = atoi(argv[38]);
			PRINT_POST_FINAL_2_OPT_VALUES = atoi(argv[40]);
			INITIAL_GENERATION_PROCEDURE = atoi(argv[42]);
			INFO_DISPLAY_MODE = atoi(argv[44]);
			SMALL_MOVE_OPERAND_USED = atoi(argv[46]);
			MTMNS_INFLUENCER_SELECTION = atoi(argv[48]);

			//MINIMUM_MOVEMENT_2OPT de pus si facut toate frumos cumva

			break;
		}
		default:
		{
			cout << "ERROR: improper number of arguments used";
			break;
		}

	}





}



int main(int argc, char *argv[])
{
	
	proces_input_arguments(argc, argv);


	srand(SEED);


	string FILE_PATH = "";
	
	FILE_NAME = "berlin52.tsp";

	int unique_id = 0;// fol pt run_test_suite;
	run_test_suite(FILE_PATH, FILE_NAME, unique_id);


	return 0;
}







