# DGSA-TSP

DGSA implementation for use with the TSP algorithm

Code is offered as is, without any waranty of any kind.



Based on paper "A discrete gravitational search algorithm for solving combinatorial optimization problems".
https://www.sciencedirect.com/science/article/pii/S0020025513006762



Parameter descriptions can be found inside the code.

Code is not written "cleanly". I know.

Larger problem at hand was path routing through antennas. For TSP use "x" and "y" and not "x_circuit" and "y_circuit".



